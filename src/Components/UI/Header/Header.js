import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.css";

const Header = () => {
  return (
    <header className="Header">
      <div className="container">
        <div className="header-inner">
          <p>My Blog</p>
          <ul>
            <li>
              <NavLink
                to="/"
                exact
                activeStyle={{
                  fontWeight: "bold",
                  color: "black",
                  textDecoration: "none",
                }}
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/posts/add"
                activeStyle={{
                  fontWeight: "bold",
                  color: "black",
                  textDecoration: "none",
                }}
              >
                Add
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/about"
                activeStyle={{
                  fontWeight: "bold",
                  color: "black",
                  textDecoration: "none",
                }}
              >
                About
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/contacs"
                activeStyle={{
                  fontWeight: "bold",
                  color: "black",
                  textDecoration: "none",
                }}
              >
                Contacs
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default Header;
