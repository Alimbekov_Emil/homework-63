import React from "react";
import "./Post.css";
import moment from "moment";
import { NavLink } from "react-router-dom";

const Post = (props) => {
  return (
    <div className="Post">
      <b>Created on :{moment(props.date).format("L LTS")}</b>
      <h3>{props.title}</h3>
      <NavLink to={"/posts/" + props.id}>Read more</NavLink>
    </div>
  );
};

export default Post;
