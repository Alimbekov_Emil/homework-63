import React, { useEffect, useState } from "react";
import axiosBlog from "../../axios-blog";
import moment from "moment";
import "./ReadMore.css";

const ReadMore = (props) => {
  const [post, setPost] = useState([]);

  const id = props.match.params.id;

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosBlog.get("/posts/" + id + ".json");
      const array = [];

      for (const key in response.data) {
        if (response.data.hasOwnProperty(key)) {
          const element = response.data[key];
          array.push({ ...element, id });
        }
      }
      setPost(array);
    };
    fetchData().catch(console.error);
  }, [id]);

  const deletePost = async (id) => {
    try {
      await axiosBlog.delete("/posts/" + id + ".json");
      props.history.push("/");
    } catch (e) {
      console.log(e);
    }
  };

  const editPost = (id) => {
    props.history.push("/edit/" + id);
  };

  const aboutPost = post.map((post) => {
    return (
      <div key={post.date} className="ReadMore">
        <p>{post.title}</p>
        <p>{post.description}</p>
        <p>{moment(post.date).format("L LTS")}</p>
        <button onClick={() => deletePost(post.id)}>Delete</button>
        <button onClick={() => editPost(post.id)}>Edit</button>
      </div>
    );
  });

  return <>{aboutPost}</>;
};

export default ReadMore;
