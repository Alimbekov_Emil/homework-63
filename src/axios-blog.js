import axios from "axios";

const axiosBlog = axios.create({
  baseURL: "https://alimbekov-lab64-default-rtdb.firebaseio.com/",
  
});

export default axiosBlog;
