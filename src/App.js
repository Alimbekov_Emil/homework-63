import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import PostEdit from "./Components/PostEdit/PostEdit";
import ReadMore from "./Components/ReadMore/ReadMore";
import Header from "./Components/UI/Header/Header";
import AboutPage from "./Container/AboutPage/AboutPage";
import AddPage from "./Container/AddPage/AddPage";
import ContacsPage from "./Container/ContacsPage/ContacsPage";
import HomePage from "./Container/HomePage/HomePage";

const App = () => (
  <BrowserRouter>
    <Header />
    <div className="container">
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/contacs" component={ContacsPage} />
        <Route path="/posts/add" component={AddPage} />
        <Route path="/posts/:id" component={ReadMore} />
        <Route path="/edit/:id" component={PostEdit} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
