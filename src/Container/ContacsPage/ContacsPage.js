import React from "react";
import "./ContacsPage.css";

const ContacsPage = () => {
  return (
    <div className="ContacsPage">
      <p>Обязательно укажиете Свое Имя и Email</p>
      <p>
        Спасибо большое за обратную связь. Мы Будем рады почитать Ваши истории!
        Каждый Ваш Комментарий Очень важен для Нас
      </p>

      <form className="FormBlock">
        <input
          className="Input"
          type="text"
          name="name"
          placeholder="Your Name"
        />
        <input
          className="Input"
          type="text"
          name="email"
          placeholder="Your Email"
        />
        <input
          className="Input"
          type="text"
          name="comment"
          placeholder="Your Comment"
        />

        <button>Отправить</button>
      </form>
    </div>
  );
};

export default ContacsPage;
