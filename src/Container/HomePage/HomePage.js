import React, { useEffect, useState } from "react";
import axiosBlog from "../../axios-blog";
import Post from "../../Components/Post/Post";
import "./HomePage.css";

const HomePage = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosBlog.get("/posts.json");
      const array = [];

      if (response.data !== null) {
        const responseKey = Object.keys(response.data);
        responseKey.forEach((id) => {
          const amount = response.data[id];
          for (const key in amount) {
            if (amount.hasOwnProperty(key)) {
              const element = amount[key];
              array.push({ ...element, id });
            }
          }
        });
      }
      setPosts(array);
    };
    fetchData().catch(console.error);
  }, []);

  const postsBlock = posts.map((post) => {
    return (
      <Post
        key={post.id}
        title={post.title}
        description={post.description}
        date={post.date}
        id={post.id}
      />
    );
  });

  return <div className="HomePage">{postsBlock}</div>;
};

export default HomePage;
