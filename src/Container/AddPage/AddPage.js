import React, { useState } from "react";
import axiosBlog from "../../axios-blog";
import Spinner from "../../Components/UI/Spinner/Spinner";
import "./AddPage.css";

const AddPage = (props) => {
  const [loading, setLoading] = useState(false);

  const [post, setPost] = useState({
    title: "",
    description: "",
  });

  const postaDataChanged = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    setPost((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const postHandler = async (event) => {
    event.preventDefault();
    setLoading(true);

    const posts = {
      post: { ...post, date: new Date() },
    };

    try {
      await axiosBlog.post("/posts.json", posts);
    } finally {
      setLoading(false);
      props.history.push("/");
    }
  };
  let form = (
    <form className="AddPage" onSubmit={postHandler}>
      <h3>Add New Post</h3>
      <label>
        Title
        <input
          type="text"
          name="title"
          className="Field"
          value={post.title}
          onChange={postaDataChanged}
        />
      </label>
      <label>
        Description
        <textarea
          type="text"
          name="description"
          cols="100"
          rows="10"
          className="Field"
          value={post.description}
          onChange={postaDataChanged}
        />
      </label>
      <button>Save</button>
    </form>
  );
  if (loading) {
    form = <Spinner />;
  }
  return <>{form}</>;
};

export default AddPage;
